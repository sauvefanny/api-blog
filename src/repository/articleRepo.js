import { connection } from "./connection";
import { Article } from '../entities/Article';

export class ArticleRepository {

    /**
 * Methode qui retourne tous les articles présent dans la database
 * @returns Promise
 */
    static async findAll() {
        const [rows] = await connection.query('SELECT * FROM articles');
        return rows.map(row => new Article(row['img'], row['titre'], row['contenu'], row['date'], row['id']));
    }

    /**
     * 
     * @param {string} term 
     * @returns retourne les articles qui contiennent les caractères qui sont insérer coté client dans la barre de recherche
     */
    static async search(term) {
        const [rows] = await connection.query('SELECT * FROM articles WHERE CONCAT(titre) LIKE ?', ['%' + term + '%'])
        return rows.map(row => new Article(row['img'], row['titre'], row['contenu'], row['date'], row['id']));

    }

    /**
     * 
     * @param {<Article/>} article 
     */
    static async add(article) {
        const [result] = await connection.query('INSERT INTO articles (img, titre, contenu, date) VALUES (?,?,?,?)', [article.img, article.titre, article.contenu, article.date]);
        article.id = result.insertId;

    }

    static async delete(id){

        const [rows] = await connection.execute(`DELETE FROM articles WHERE id=?`, [id]);
    }

};