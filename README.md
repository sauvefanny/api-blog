# Projet BLOG

Créer une plateforme de blog en node/react avec une gestion des utilisateurs·ices.

Un user doit pouvoir s'inscrire, se connecter et une fois connecté doit pouvoir poster des articles sur son espace personnel.
N'importe qui peut consulter les articles des autres user.

J'ai choisi ce theme car j'ai pour projet de créer un blog pour l'entreprise de ma soeur, cela m'a permis de mettre en pratique les prémices du projet.

## I/Créer un diagramme de Use Case
---

![desktop-maquette](/api-projet-blog/public/images/Diagramme%20UseCase.png)

---
## Créer une ou deux maquettes fonctionnelles
![desktop-maquette](/api-projet-blog/public/images/ajout-dun-article.png)

![desktop-maquette](/api-projet-blog/public/images/phone-maquette.png)

![desktop-maquette](/api-projet-blog/public/images/desktop-maquette.png)


---
## II/Identifier les entités:diagramme de classe
---
<img src="/api-projet-blog/public/images/entities.png" width="50%" />

---
## III/Créer la bdd et un script de mise en place
J'ai créé une base de données et importé le fichier dedans:
 >mysql -u simplon -p app-blog < db.sql

```DROP TABLE IF EXISTS users;
CREATE TABLE `users`(
  `id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `pseudo` VARCHAR (30) collate latin1_general_ci NOT NULL,
  `email` varchar(250) collate latin1_general_ci NOT NULL,
  `mdp` VARCHAR(32) collate latin1_general_ci NOT NULL,
  `role` VARCHAR (30) collate latin1_general_ci NOT NULL
);
DROP TABLE IF EXISTS articles;
CREATE TABLE `articles`(
  `id` INTEGER AUTO_INCREMENT PRIMARY KEY,
  `img` VARCHAR (255) NOT NULL,
  `titre` VARCHAR(255) NOT NULL,
  `contenu` varchar(350) NOT NULL,
  `date` DATE,
  `users` int,
  FOREIGN KEY (`id`) REFERENCES `users`(`id`)
);

INSERT INTO `users` (pseudo,email,mdp,role) VALUES
('begonnia', 'begonnia@test.com', '1234', 'user');
INSERT INTO `articles` (img,titre,contenu,date)
VALUES('https://cdn.pixabay.com/photo/2012/03/01/00/55/garden-19830_640.jpg','Les plantes melliferes','Attirer les insectes pollinisateurs au jardin, et notamment les abeilles, c\est facile, en privilégiant les plantes mellifères. Les espèces mellifères sont nombreuses. Un gestes pour la biodiversité, un bon point pour le jardin',12/10/2021);

```

## VI/Créer une API avec node/express
---
### Création du server:

Utilisation du Framework Express comme infrastructure car il aide à résoudre de nombreux problèmes courants. Exemples de verbes HTTP: 
* get(indique que l'utilisateur veut lire les données de la ressource), 
* post(un utilisateur souhaite écrire des données), 
* post(créer une nouvelle ressource afin de décrire ce que le client veut faire).
* Pour retourner du texte brut, utiliser la méthode : send()(ex:res.send()), 
* pour des données de type JSON: (ex: res.json())

```
export const server = express();
server.use(express.json());
server.use(cors());

/**
 * test
 */
server.get('/', async (req, res) => {
    res.json('Bienvenue')
});

server.use('/api/blog/user', userController);
server.use('/api/blog/article', articleController);
```
Une fois le port défini ici au sein du dossier app.js l'application est prête à recevoir des requêtes. Le point .env incrit dans le gitignore permet de faire le lien avec la base de données.(ex:DATABASE_URL=mysql://simplon:1234@localhost:3306/app-blog)

### Les requêtes:création de la structure des dossiers:

#### Entités:
___
Dans ce fichier on crée un objet qui contient un constructeur afin de pouvoir l'instancier

#### Controller: permet d'extraire les routes dans un module
___
```articleController.get('/', async (req,res)=>{
    try {
        let articles;
        if(req.query.search){
            articles = await ArticleRepository.search(req.query.search);

        }   else {
            articles = await ArticleRepository.findAll();

        }
        res.json(articles);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})
```

#### Repository: module qui contient nos requêtes sql selon les bdd
___
```export class ArticleRepository {

    /**
 * Methode qui retourne tous les articles présent dans la database
 * @returns Promise
 */
    static async findAll() {
        const [rows] = await connection.query('SELECT * FROM articles');
        return rows.map(row => new Article(row['img'], row['titre'], row['contenu'], row['date'], row['id']));
    }
```
---
## V/Faire une authentification avec hashing et JWT
---
* npm install package:
- express, dotenv;
- passport pour la protection des routes
- passport-jwt pour la protection par token
- jsonwebtoken pour créer et décoder les tokens
- bcrypt pour hasher le mot de passe

* J'ai créé un dossier config dans lequel deux commandes m'ont permis de générer deux clefs JWT (JasonWebToken) une public une privée:

```
ssh-keygen -t rsa -P "" -b 4096 -m PEM -f config/id_rsa
ssh-keygen -e -m PEM -f config/id_rsa > config/id_rsa.pub
```

* * On récupérera ces clefs dans le code en utilisant fs

```
const privateKey = fs.readFileSync('config/id_rsa');
const publicKey = fs.readFileSync('config/id_rsa.pub');
```

* Ces actions m'ont permis de créer un dossier utils avec un fichier token.js. Il contient des fixtures permettant de générer un token. 

```
export function generateToken(payload, expire = 60*60) {
    const token = jwt.sign(payload, privateKey,{algorithm: 'RS256', expiresIn: expire });
    return token;
}
```

* npm i passport => Passport est un middleware d'authentification pour Node.(ajouter une stratégie d'authentification par Token)

>une fonction
```
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }, async (payload, done) => {
        
        try {
            const user = await UserRepository.findByEmail(payload.email);
            
            if(user) {
               
                return done(null, user);
            }
            
            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}
```

>dans le fichier server.js

```
configurePassport();

export const server = express();

server.use(passport.initialize());
```
[Lien vers la doc passport.js](https://www.passportjs.org)

Pour tester j'ai utilisé Thunder Client:
* requête post vers api en lui donnant un email et password existant dans la bdd.
* On récupère le token dans la réponse du login, et on fait une autre requête en GET.

 >nous stockons le jeton Web JSON (JWT) côté client : stockage local dans le navigateur avec une expiration.

* Vérifier si un user avec cet email ou username existe déjà

 ```
const exists = await UserRepository.findByEmail(req.body.email);
if(exists) {
    res.status(400).json({error: 'Email already taken'});
    return;
}
```

.Assigner les valeurs par défaut et hasher le mot de passe avec bcrypt 
```
newUser.role = 'user';
newUser.password = await bcrypt.hash(newUser.password, 11);
await UserRepository.add(newUser);
```
[Lien vers la doc du JWT](https://jwt.io/)

* On va chercher le user par son email dans la Bdd.
* Si le user existe, on compare le mot de passe envoyé avec le mot de passe hashé stocké en bdd
* Si tout est ok, on renvoie le user et surtout le token.

---
## Créer une interface avec React
---

>création d'un projet react
npm create-react-app api-projet-blog

Sur ce projet j'installe :
* react-router-dom
* react bootstrap
* axios
* react-redux
* reduxjs/toolkit 
  
[Lien vers la doc redux toolkit](https://redux-toolkit.js.org/)


>La slice redux pour le user

Permet de stocker le user de manière globale dans l’application.
Les parties feedback ne sont nécessaires que si on gère également les requêtes AJAX depuis redux.

```
const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        loginFeedback: '',
        registerFeedback: ''
    },
    reducers: {
        login(state, {payload}) {
            state.user = payload;
        },
        logout(state) {
            state.user = null;
            localStorage.removeItem('token');
        },
        updateLoginFeedback(state, {payload}) {
            state.loginFeedback = payload
        },
        updateRegisterFeedback(state, {payload}) {
            state.registerFeedback = payload
        }
    }
});

export const {login,logout, updateLoginFeedback, updateRegisterFeedback} = authSlice.actions;

export default authSlice.reducer
```
>Création du store
On lui assigne toutes les slices qu’on aura créé.
Afin que le store soit accessible depuis tous les components, il faut entourer notre app d'un Provider.

<Provider store={store}>
</Provider>

```
export const store = configureStore({
    reducer: {
        auth: authSlice
    }
});
```
>Les thunks:
fonction asynchrone de redux dans le cadre de l'authentification j'en ai fais 3:

. loginWithToken afin d’authentifier le user si on a un token stocké en localStorage.

. loginWithCredentials: connection à partir d'un email et mot de passe

. le register: création d'un compte 


>Utilisation au sein d'un components
Pour muter la valeur/déclencher une action ou un thunk, on utilisera
```
const dispatch = useDispatch();
dispatch(login(values)

const user = useSelector(state => state.auth.user)
```

>Axios et les intercepteurs,
au sein du dossier service:

* Servent à injecter le token dans les headers des requêtes sortantes ainsi que pour déconnecter le user si le token expire.
```
axios.interceptors.request.use((config) => {
    const token =localStorage.getItem('token');
    if(token) {

        config.headers.authorization = 'bearer '+token;
    }
    return config;
});

axios.interceptors.response.use((response) => {

    if(response.status === 401) {
        store.dispatch(logout("temps d'expiration"));
    }

    return response;
})
```
\
[Lien GitLab](https://gitlab.com/sauvefanny);\
[Lien vers le deployement](à venir);