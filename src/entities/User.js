export class User {

    id;
    pseudo;
    email;
    mdp;
    role;
    /**
     * @param {*} id 
     * @param {string} pseudo 
     * @param {string} email 
     * @param {number} mdp 
     * @param {string} role 
     */
    constructor(pseudo, email, mdp, role,id = null){

        this.pseudo = pseudo;
        this.email = email;
        this.mdp = mdp;
        this.role = role;
        this.id = id;
        
    }
};