export class Article {

    id;
    img;
    titre;
    contenu;
    date;

    /**
     * @param {number} id 
     * @param {string} img 
     * @param {string} titre 
     * @param {string} contenu 
     * @param {date} date 
     */
    constructor(img, titre, contenu, date, id = null) {

        this.img = img;
        this.titre = titre;
        this.contenu = contenu;
        this.date = date;
        this.id = id;

    }
};