import express from 'express';
import cors from 'cors';
import { userController } from './controller/userController';
import { articleController } from './controller/articleController';
import { configurePassport } from './utils/token';
import passport from 'passport';



configurePassport();
export const server = express();

server.use(passport.initialize());

server.use(express.json());
server.use(cors());
// /**
//  * Test
//  */
// server.get('/', async (req, res) => {
//     res.json('Bienvenue')
// });

server.use('/api/user', userController);
server.use('/api/blog/article', articleController);
