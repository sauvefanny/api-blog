import { connection } from "./connection";
import { User } from '../entities/User';


export class UserRepository {
    /**
     * Methode pour trouver un user par son email dans la bd
     * @param {string} email 
     * @returns {Promise<User>}
     */
    static async findByEmail(email) {
        const [rows] = await connection.query('SELECT * FROM users WHERE email=?', [email]);

        if (rows.length === 1) {

            return new User(rows[0].pseudo, rows[0].email, rows[0].mdp, rows[0].role, rows[0].id);
        }
        return null;

    }
    /**
     * Permet de faire persister un utilisateur au sein de la bd
     * @param {User} user 
     */
    static async add(user) {

        const [rows] = await connection.query('INSERT INTO users (pseudo,email, mdp, role) VALUES (?,?,?,?)', [user.pseudo, user.email, user.mdp, user.role,]);
        user.id = rows.insertId;
    }

    /**
     * Methode qui retourne tous les utilisateurs présent dans la base e donnée
     * @returns Promise des Users
     */
    static async findAll() {
        const [rows] = await connection.query('SELECT * FROM users');
        return rows.map(row => new User(row['pseudo'], row['email'], row['mdp'], row['role'], row['id']));
    }

};