import { Router } from "express";
import { uploader } from "../uploader";
import { Article } from "../entities/Article";
import { ArticleRepository } from "../repository/articleRepo";

export const articleController = Router();

articleController.get('/', async (req,res)=>{
    try {
        let articles;
        if(req.query.search){
            articles = await ArticleRepository.search(req.query.search);

        }   else {
            articles = await ArticleRepository.findAll();

        }
        res.json(articles);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
})

articleController.post('/', uploader.single('img'), async (req, res) => {
    try {
        let newArticle= new Article();
        Object.assign(newArticle, req.body); 
        
        await ArticleRepository.add(newArticle)

        res.status(201).json(newArticle);
    }catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
})

articleController.delete('/:id', async (req, res) => {
    try {
        await ArticleRepository.delete(req.params.id);
        res.status(204).end()
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message: 'Server Error'
        })
    }
});



